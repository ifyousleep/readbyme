package com.ifyou.readbyme

import android.app.Application
import android.support.v7.app.AppCompatDelegate
import com.ifyou.readbyme.utils.TypefaceUtil

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        TypefaceUtil.overrideFont(applicationContext, "SERIF", "fonts/Roboto.ttf")
    }
}