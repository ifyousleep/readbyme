package com.ifyou.readbyme.helpers

import android.content.SharedPreferences
import android.content.Context


class PreferencesHelper(private val mContext: Context) {

    companion object {
        private val PREFERENCES_NAME = "readbyme_preferences"
        private val KEY_SORT = "sort_by"
        private val GOOGLE_NAME = "google_name"
        private val GOOGLE_EMAIL = "google_email"
        private val GOOGLE_PHOTO = "google_photo"
        private val GOOGLE_SIGN = "google_sign"
        private val KEY_FIRST = "key_first"
    }

    private val sharedPreferences: SharedPreferences
        get() = mContext.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)

    var googleSign: Boolean
        get() = sharedPreferences.getBoolean(GOOGLE_SIGN, false)
        set(isSign) = sharedPreferences.edit().putBoolean(GOOGLE_SIGN, isSign).apply()

    var firstOpen: Boolean
        get() = sharedPreferences.getBoolean(KEY_FIRST, true)
        set(open) = sharedPreferences.edit().putBoolean(KEY_FIRST, open).apply()

    var sortBy: Int
        get() = sharedPreferences.getInt(KEY_SORT, 0)
        set(sort) = sharedPreferences.edit().putInt(KEY_SORT, sort).apply()

    var googleName: String
        get() = sharedPreferences.getString(GOOGLE_NAME, "")
        set(name) = sharedPreferences.edit().putString(GOOGLE_NAME, name).apply()

    var googleEmail: String
        get() = sharedPreferences.getString(GOOGLE_EMAIL, "")
        set(email) = sharedPreferences.edit().putString(GOOGLE_EMAIL, email).apply()

    var googlePhoto: String
        get() = sharedPreferences.getString(GOOGLE_PHOTO, "")
        set(photo_url) = sharedPreferences.edit().putString(GOOGLE_PHOTO, photo_url).apply()
}