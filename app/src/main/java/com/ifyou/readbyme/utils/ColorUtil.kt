package com.ifyou.readbyme.utils

import android.content.Context
import android.graphics.Color
import com.ifyou.readbyme.R

fun randomColor(context: Context): Int {
    val colors = context.resources.obtainTypedArray(R.array.material_colors)
    val index = (Math.random() * colors.length()).toInt()
    val tintColor = colors.getColor(index, Color.RED)
    colors.recycle()
    return tintColor
}
