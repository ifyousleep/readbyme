package com.ifyou.readbyme.activities

import android.os.Bundle
import com.codemybrainsout.onboarder.AhoyOnboarderActivity
import com.codemybrainsout.onboarder.AhoyOnboarderCard
import com.ifyou.readbyme.R
import java.util.*

class IntroActivity : AhoyOnboarderActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val ahoyCard1 = AhoyOnboarderCard(getString(R.string.intro_favorite), getString(R.string.intro_favorite_long), R.drawable.intro1)
        val ahoyCard2 = AhoyOnboarderCard(getString(R.string.intro_read), getString(R.string.intro_read_long), R.drawable.intro2)
        val ahoyCard3 = AhoyOnboarderCard(getString(R.string.intro_backup), getString(R.string.intro_backup_long), R.drawable.intro3)

        ahoyCard1.setBackgroundColor(R.color.colorIntro)
        ahoyCard2.setBackgroundColor(R.color.colorIntro)
        ahoyCard3.setBackgroundColor(R.color.colorIntro)

        val pages = ArrayList<AhoyOnboarderCard>()

        pages.add(ahoyCard1)
        pages.add(ahoyCard2)
        pages.add(ahoyCard3)

        for (page in pages) {
            page.setTitleColor(R.color.black)
            page.setDescriptionColor(R.color.grey_600)
        }

        setFinishButtonTitle(getString(R.string.finish))
        showNavigationControls(false)

        val colorList = ArrayList<Int>()
        colorList.add(R.color.colorToolbarWas)
        colorList.add(R.color.colorToolbar)
        colorList.add(R.color.colorPrimary)

        setColorBackground(colorList)

        setOnboardPages(pages)

    }

    override fun onFinishButtonPressed() {
        finish()
    }

    override fun onBackPressed() {

    }
}
