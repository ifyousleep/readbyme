package com.ifyou.readbyme.activities

import android.content.Intent
import android.content.IntentSender
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.ifyou.readbyme.R
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.ifyou.readbyme.preferences.Preferences
import kotlinx.android.synthetic.main.activity_drive.*
import android.content.IntentSender.SendIntentException
import android.view.View
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.drive.*
import java.io.*
import java.text.DateFormat
import com.google.android.gms.drive.query.Filters
import com.google.android.gms.drive.query.SearchableField
import com.google.android.gms.drive.query.Query
import com.ifyou.readbyme.db.DatabaseHelper
import org.jetbrains.anko.toast
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener

class DriveActivity : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    private lateinit var mGoogleApiClient: GoogleApiClient
    private val REQUEST_CODE_CREATOR = 1
    private val RC_SIGN_IN = 9001
    private var mDbFile: File? = null
    private val DATABASE_NAME = "ReadByMe.db"

    companion object {
        const val REQUEST_CODE_RESOLUTION: Int = 3
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drive)

        toolbar.title = getString(R.string.menu_sync)
        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        val sqlHelper = DatabaseHelper(this)
        mDbFile = File(getDatabasePath(sqlHelper.databaseName).toURI())

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(Drive.SCOPE_FILE)
                .requestEmail()
                .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this, onConnectionFailedListener)
                .addOnConnectionFailedListener(onConnectionFailedListener)
                .addConnectionCallbacks(this)
                .addApi(Drive.API)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()

        sign_in.setOnClickListener {
            signIn()
        }

        sign_out.setOnClickListener {
            signOut()
        }

        loadFromDrive.setOnClickListener {
            sign_in.text = getString(R.string.preparations)
            updateLocalDb()
        }

        loadToDrive.setOnClickListener {
            sign_in.text = getString(R.string.preparations)
            deleteDb()
            updateDriveDb(mDbFile!!)
        }

        if (Preferences.getPreferences(this).googleSign) {
            sign_in.text = getString(R.string.loading)
            sign_in.isEnabled = false
            showProfile()
        }
    }

    private fun signIn() {
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun signOut() {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback {
            Preferences.getPreferences(this).googleName = ""
            Preferences.getPreferences(this).googleEmail = ""
            Preferences.getPreferences(this).googlePhoto = ""
            Preferences.getPreferences(this).googleSign = false
            finish()
        }
    }

    override fun onStart() {
        super.onStart()
        if (Preferences.getPreferences(this).googleSign) {
            val opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient)
            if (opr.isDone) {
                val result = opr.get()
                handleSignInResult(result)
            } else {
                sign_in.text = getString(R.string.loading)
                opr.setResultCallback {
                    googleSignInResult ->
                    sign_in.text = getString(R.string.good_day)
                    handleSignInResult(googleSignInResult)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSignInResult(result)
        }
        if (requestCode == REQUEST_CODE_CREATOR && resultCode == RESULT_OK) {
            toast(getString(R.string.success))
            sign_in.text = getString(R.string.success)
            getSizeAndDateInDrive()
        }
    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        if (result.isSuccess) {
            Preferences.getPreferences(this).googleSign = true
            sign_in.text = getString(R.string.good_day)
            val acct = result.signInAccount
            val personName = acct!!.displayName
            val personPhotoUrl = acct.photoUrl.toString()
            val email = acct.email
            nav_title.text = personName
            nav_subtitle.text = email
            Glide.with(applicationContext).load(personPhotoUrl)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageViewAvatar)
            Preferences.getPreferences(this).googleName = acct.displayName!!
            Preferences.getPreferences(this).googleEmail = acct.email!!
            Preferences.getPreferences(this).googlePhoto = acct.photoUrl.toString()
            loadFromDrive.visibility = View.VISIBLE
            loadToDrive.visibility = View.VISIBLE
            divider.visibility = View.VISIBLE
            divider2.visibility = View.VISIBLE
            sign_out.visibility = View.VISIBLE
            showProfile()
            getSizeAndDateInDrive()
            getSizeAndDateInLocal()
        } else {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback {
                Preferences.getPreferences(this).googleName = ""
                Preferences.getPreferences(this).googleEmail = ""
                Preferences.getPreferences(this).googlePhoto = ""
                Preferences.getPreferences(this).googleSign = false
                recreate()
            }
        }
    }


    override fun onConnected(bundle: Bundle?) {

    }

    override fun onConnectionSuspended(reason: Int) {
        throw UnsupportedOperationException()
    }

    override fun onConnectionFailed(result: ConnectionResult) {
        if (!result.hasResolution()) {
            GoogleApiAvailability.getInstance().getErrorDialog(this, result.errorCode, 0).show()
            return
        }
        try {
            result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION)
        } catch (ex: IntentSender.SendIntentException) {

        }
    }

    private var onConnectionFailedListener: GoogleApiClient.OnConnectionFailedListener = OnConnectionFailedListener { connectionResult ->
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, REQUEST_CODE_RESOLUTION)
            } catch (e: IntentSender.SendIntentException) {

            }
        } else {
            GoogleApiAvailability.getInstance().getErrorDialog(this, connectionResult.errorCode, 0).show()
        }
    }

    override fun onResume() {
        super.onResume()
        if (Preferences.getPreferences(this).googleSign)
            mGoogleApiClient.connect()
    }

    override fun onPause() {
        super.onPause()
        if (Preferences.getPreferences(this).googleSign)
            mGoogleApiClient.disconnect()
    }

    private fun showProfile() {
        Glide.with(applicationContext).load(Preferences.getPreferences(this).googlePhoto)
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageViewAvatar)
        nav_title.text = Preferences.getPreferences(this).googleName
        nav_subtitle.text = Preferences.getPreferences(this).googleEmail
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getSizeAndDateInDrive() {
        val query = Query.Builder()
                .addFilter(Filters.eq(SearchableField.TITLE, DATABASE_NAME))
                .build()
        Drive.DriveApi.query(mGoogleApiClient, query)
                .setResultCallback({ result ->
                    if (result.metadataBuffer.count > 0) {
                        val sizeBd = result.metadataBuffer.get(0).fileSize.div(1024)
                        var value: String
                        val dateBd = "(" + DateFormat.getDateInstance().format(result.metadataBuffer.get(0).modifiedDate) + ")"
                        if (sizeBd >= 1024)
                            value = (sizeBd / 1024).toString() + " Mb"
                        else
                            value = sizeBd.toString() + " Kb"
                        value += " " + dateBd
                        sizeAndDateInDrive.text = value
                    } else
                        sizeAndDateInDrive.text = getString(R.string.not_found_backup)
                })
    }

    private fun getSizeAndDateInLocal() {
        val sizeBd = mDbFile?.length()?.div(1024)
        var value: String
        if (sizeBd!! >= 1024)
            value = (sizeBd / 1024).toString() + " Mb"
        else
            value = sizeBd.toString() + " Kb"
        val dateBd = "(" + DateFormat.getDateInstance().format(mDbFile?.lastModified()) + ")"
        value += " " + dateBd
        sizeAndDateInLocal.text = value
    }

    private fun updateLocalDb() {
        val query = Query.Builder()
                .addFilter(Filters.eq(SearchableField.TITLE, DATABASE_NAME))
                .build()
        Drive.DriveApi.query(mGoogleApiClient, query)
                .setResultCallback({ result ->
                    if (result.metadataBuffer.count > 0) {
                        val driveId = result.metadataBuffer.get(0).driveId
                        val driveFile = driveId.asDriveFile()
                        driveFile.open(mGoogleApiClient, DriveFile.MODE_READ_ONLY, null)
                                .setResultCallback({ result ->
                                    if (result.status.isSuccess) {
                                        val contents = result.driveContents
                                        val iStream = contents.inputStream
                                        val buffer: ByteArray
                                        try {
                                            buffer = ByteArray(iStream.available())
                                            var bytesRead: Int
                                            val output = ByteArrayOutputStream()
                                            while (true) {
                                                bytesRead = iStream.read(buffer, 0, buffer.size)
                                                if (bytesRead == -1) {
                                                    break
                                                }
                                                output.write(buffer, 0, bytesRead)
                                            }
                                            val byteArray = output.toByteArray()
                                            val fos = FileOutputStream(mDbFile, false) //true = append, false = overwrite
                                            fos.write(byteArray)
                                            fos.close()
                                            toast(getString(R.string.successfully_recovered))
                                            sign_in.text = getString(R.string.success)
                                            getSizeAndDateInLocal()
                                        } catch (e: IOException) {
                                            e.printStackTrace()
                                        }
                                        contents.discard(mGoogleApiClient)
                                    }
                                })
                        result.release()
                    } else {
                        toast(getString(R.string.not_found_backup))
                        sign_in.text = getString(R.string.not_found_backup)
                    }
                })
    }

    private fun deleteDb() {
        val driveId = arrayOfNulls<String>(1)
        val query = Query.Builder()
                .addFilter(Filters.eq(SearchableField.TITLE, DATABASE_NAME))
                .build()
        Drive.DriveApi.query(mGoogleApiClient, query)
                .setResultCallback({ result ->
                    if (result.metadataBuffer.count > 0) {
                        driveId[0] = result.metadataBuffer.get(0).driveId.encodeToString()
                        val driveFile = DriveId.decodeFromString(driveId[0]).asDriveFile()
                        driveFile.delete(mGoogleApiClient)
                    }
                    result.release()
                }
                )
    }

    private fun updateDriveDb(fileToSave: File) {
        Drive.DriveApi.newDriveContents(mGoogleApiClient).setResultCallback({ result ->
            if (result.status.isSuccess) {
                val outputStream = result.driveContents.outputStream
                val bArray = file2Bytes(fileToSave)
                try {
                    outputStream.write(bArray)
                } catch (e1: IOException) {
                    toast(getString(R.string.can_not_upload))
                }

                val metadataChangeSet = MetadataChangeSet.Builder()
                        .setMimeType("application/x-sqlite3")
                        .setTitle(DATABASE_NAME)
                        .build()
                val intentSender = Drive.DriveApi
                        .newCreateFileActivityBuilder()
                        .setInitialMetadata(metadataChangeSet)
                        .setInitialDriveContents(result.driveContents)
                        .build(mGoogleApiClient)
                try {
                    startIntentSenderForResult(intentSender, REQUEST_CODE_CREATOR, null, 0, 0, 0)
                } catch (e: SendIntentException) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun file2Bytes(file: File?): ByteArray? {
        if (file != null)
            try {
                return is2Bytes(FileInputStream(file))
            } catch (ignore: Exception) {
                ignore.printStackTrace()
            }

        return null
    }

    fun is2Bytes(iStream: InputStream?): ByteArray {
        var buf: ByteArray? = null
        var bufIS: BufferedInputStream? = null
        if (iStream != null)
            try {
                val byteBuffer = ByteArrayOutputStream()
                bufIS = BufferedInputStream(iStream)
                buf = ByteArray(iStream.available())
                var cnt: Int
                while (true) {
                    cnt = iStream.read(buf, 0, buf.size)
                    if (cnt == -1) {
                        break
                    }
                    byteBuffer.write(buf, 0, cnt)
                }
                buf = if (byteBuffer.size() > 0) byteBuffer.toByteArray() else null
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                try {
                    if (bufIS != null) bufIS.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        return buf!!
    }
}