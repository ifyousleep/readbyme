package com.ifyou.readbyme.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.Menu
import android.view.MenuItem
import android.content.Intent
import android.app.Dialog
import android.content.pm.PackageManager
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.github.javiersantos.piracychecker.PiracyChecker
import com.github.javiersantos.piracychecker.enums.InstallerID
import com.ifyou.readbyme.BuildConfig
import com.ifyou.readbyme.fragments.BooksFragment
import com.ifyou.readbyme.preferences.Preferences
import com.ifyou.readbyme.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var tableName = "was_read"
    private var menuId = 0
    private var sortBy = 0
    private var menu: Menu? = null
    private val ADD_BOOK_ID = 10001
    private val UPDATE_FRAGMENT_ID = 10002
    private val SETTINGS_ID = 10003
    private val MARKET_URL = "https://play.google.com/store/apps/details?id="

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        /*val p = fab.layoutParams as CoordinatorLayout.LayoutParams
        p.behavior = FABScrollingBehavior()
        fab.layoutParams = p*/
        fab.setOnClickListener {
            startAddBook()
        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        updateNavView()

        if (savedInstanceState == null) {
            createWasReadFragment()
            if (Preferences.getPreferences(this).firstOpen) {
                val intent = Intent(this, IntroActivity::class.java)
                startActivity(intent)
                Preferences.getPreferences(this).firstOpen = false
            }
        } else {
            supportActionBar?.title = savedInstanceState.getString("title")
            tableName = savedInstanceState.getString("table")
            setStatusBarAndToolbarColor()
        }

        if (!BuildConfig.DEBUG)
            checkLicense()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString("title", supportActionBar?.title.toString())
        outState.putString("table", tableName)
        super.onSaveInstanceState(outState)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ADD_BOOK_ID || requestCode == SETTINGS_ID) {
            createWasOrWill()
        } else if (requestCode == UPDATE_FRAGMENT_ID) {
            val fragment: Fragment = supportFragmentManager.findFragmentById(R.id.container)
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun createWasOrWill() {
        when (tableName) {
            "was_read" -> createWasReadFragment()
            "will_read" -> createWillReadFragment()
        }
    }

    private fun createWasReadFragment() {
        showOverflowMenu(true)
        tableName = "was_read"
        setStatusBarAndToolbarColor()
        sortBy = Preferences.getPreferences(this).sortBy
        val existing = supportFragmentManager.findFragmentById(R.id.container)
        if (existing != null) {
            supportFragmentManager.beginTransaction().remove(existing).commit()
        }
        supportActionBar?.title = getString(R.string.was_read)
        supportFragmentManager.beginTransaction()
                .add(R.id.container, BooksFragment.newInstance(tableName, sortBy))
                .commit()
        fab.show()
    }

    private fun createWillReadFragment() {
        showOverflowMenu(false)
        tableName = "will_read"
        setStatusBarAndToolbarColor()
        val existing = supportFragmentManager.findFragmentById(R.id.container)
        if (existing != null) {
            supportFragmentManager.beginTransaction().remove(existing).commit()
        }
        supportActionBar?.title = getString(R.string.will_read)
        supportFragmentManager.beginTransaction()
                .add(R.id.container, BooksFragment.newInstance(tableName))
                .commit()
        fab.show()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        this.menu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_sort -> {
                onCreateDialog()?.show()
                return true
            }
            R.id.action_about -> {
                AlertDialog.Builder(this)
                        .setTitle(getString(R.string.app_name))
                        .setMessage(getString(R.string.action_about))
                        .show()
                return true
            }
            R.id.action_help -> {
                val intent = Intent(this, IntroActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onCreateDialog(): Dialog? {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.sort)
        builder.setItems(R.array.sort_actions) { dialog, which ->
            sortBy = which
            Preferences.getPreferences(this).sortBy = sortBy
            createWasReadFragment()
        }
        return builder.create()
    }

    override fun onResume() {
        super.onResume()
        setStatusBarAndToolbarColor()
        updateNavView()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_was_read -> {
                menuId = 0
                createWasReadFragment()
            }
            R.id.nav_will_read -> {
                menuId = 1
                createWillReadFragment()
            }
            R.id.nav_sync -> {
                val intent = Intent(this, DriveActivity::class.java)
                startActivity(intent)
            }
            R.id.nav_settings -> {
                val intent = Intent(this, SettingsActivity::class.java)
                startActivityForResult(intent, SETTINGS_ID)
            }
            R.id.nav_rate -> {
                val rateIntent = Intent(Intent.ACTION_VIEW, Uri.parse(MARKET_URL + packageName))
                startActivity(rateIntent)
            }
        }
        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun showOverflowMenu(showMenu: Boolean) {
        if (menu == null)
            return
        menu?.setGroupVisible(R.id.main_menu_group, showMenu)
    }

    private fun getVersionInfo(): String {
        var versionName = ""
        try {
            val packageInfo = packageManager.getPackageInfo(packageName, 0)
            versionName = packageInfo.versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        return versionName
    }

    private fun updateNavView() {
        val gName = Preferences.getPreferences(this).googleName
        val gEmail = Preferences.getPreferences(this).googleEmail
        val gPhoto = Preferences.getPreferences(this).googlePhoto
        if (gName.isEmpty())
            nav_view.getHeaderView(0).nav_title.text = getString(R.string.app_name)
        else
            nav_view.getHeaderView(0).nav_title.text = gName
        if (gEmail.isEmpty())
            nav_view.getHeaderView(0).nav_subtitle.text = String.format(getString(R.string.version), getVersionInfo())
        else
            nav_view.getHeaderView(0).nav_subtitle.text = gEmail
        if (gPhoto.isNotEmpty())
            Glide.with(applicationContext).load(gPhoto)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(nav_view.getHeaderView(0).imageViewAvatar)
        else
            Glide.with(applicationContext).load(R.drawable.square)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(nav_view.getHeaderView(0).imageViewAvatar)
    }

    private fun setStatusBarAndToolbarColor() {
        val colorT: Int
        val colorS: Int
        when (tableName) {
            "was_read" -> {
                colorS = R.color.colorStatusWas
                colorT = R.color.colorToolbarWas
            }
            else -> {
                colorS = R.color.colorStatus
                colorT = R.color.colorToolbar
            }
        }
        drawer_layout.setStatusBarBackgroundColor(ContextCompat.getColor(this, colorS))
        drawer_layout.setScrimColor(ContextCompat.getColor(this, R.color.colorTransparent))
        val colorDrawable = ColorDrawable(ContextCompat.getColor(this, colorT))
        supportActionBar?.setBackgroundDrawable(colorDrawable)
        nav_view.getHeaderView(0).nav_header.background = colorDrawable

        nav_view.itemTextColor = null
        nav_view.itemIconTintList = null
        val menuItem = nav_view.menu.getItem(menuId)
        val s = SpannableString(menuItem.title)
        s.setSpan(ForegroundColorSpan(ContextCompat.getColor(this, colorT)), 0, s.length, 0)
        menuItem.title = s

        val id: Int
        when (menuId) {
            0 -> id = 1
            else -> id = 0
        }

        val menuItemWas = nav_view.menu.getItem(id)
        val p = SpannableString(menuItemWas.title)
        p.setSpan(ForegroundColorSpan(ContextCompat.getColor(this, R.color.colorBlack)), 0, p.length, 0)
        menuItemWas.title = p
    }

    private fun checkLicense() {
        PiracyChecker(this)
                .enableInstallerId(InstallerID.GOOGLE_PLAY)
                .enableGooglePlayLicensing("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnb3iXEVmcoXtwDxmX+i5BJ93he9zdBH87JhlB1dCPYAgSw+cTP3CGSlmTuLk2HnJCZpZNE8XrflXwDSr8fk4uffj0wRN+XxOs9/VHxfplguaCL5nxS7LEyPb2RXpUIxkgNqpfuTrWjlWX8lJsFxLqiCeoVCBByOIK+6l7zwtoiLBnf2iNygXV7F4cfrfSmqkA7F/VMwJNuoK36a+AvD3F/PicJaDwXmt/hHySrV5IcXiVc3zXmTsM0Reup22St1gUU9E28Tq4Nwekc8vYNYc2G1cFJnNH/RoOelOxpPS2IqSn1NeBcGQvCj6Fez6/idrVbQaD7p6l9jQkQ31dZZdnQIDAQAB")
                .start()
    }

    private fun startAddBook() {
        val intent = Intent(this, AddBookActivity::class.java)
        intent.putExtra("tableName", tableName)
        startActivityForResult(intent, ADD_BOOK_ID)
    }
}
