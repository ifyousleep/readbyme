package com.ifyou.readbyme.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.ifyou.readbyme.R
import kotlinx.android.synthetic.main.activity_add_book.*
import android.view.WindowManager
import com.ifyou.readbyme.db.DatabaseHelper
import com.ifyou.readbyme.db.BookModel
import com.ifyou.readbyme.db.DatabaseQueryHelper
import com.ifyou.readbyme.utils.OnTextChanged

class AddBookActivity : AppCompatActivity() {

    private var tableName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_book)

        val extras = intent.extras

        if (extras != null) {
            tableName = extras.getString("tableName")
        } else
            finish()

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        toolbar.title = getString(R.string.add_book)
        setSupportActionBar(toolbar)

        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        input_name.addTextChangedListener(OnTextChanged(afterTextChanged = { validateName() }))
        input_title.addTextChangedListener(OnTextChanged(afterTextChanged = { validateTitle() }))
        input_year.addTextChangedListener(OnTextChanged(afterTextChanged = { validateYear() }))

        fab.setOnClickListener {
            submitForm()
        }
    }

    private fun submitForm() {
        if (!validateName()) {
            return
        }
        if (!validateTitle()) {
            return
        }
        if (!validateYear()) {
            return
        }
        addBook()
    }

    private fun addBook() {
        val sqLiteHelper = DatabaseQueryHelper()
        val db = DatabaseHelper(this).writableDatabase
        sqLiteHelper.insertItem(db, BookModel(
                input_name.text.toString(), input_title.text.toString(), input_year.text.toString(), 0
        ), tableName)
        db.close()
        /*val intentParent = intent
        setResult(Activity.RESULT_OK, intentParent)*/
        finish()
    }

    private fun requestFocus(view: View) {
        if (view.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    private fun validateName(): Boolean {
        if (input_name.text.toString().trim().isEmpty()) {
            input_layout_name.error = getString(R.string.err_msg_name)
            requestFocus(input_name)
            return false
        } else
            input_layout_name.isErrorEnabled = false
        return true
    }

    private fun validateTitle(): Boolean {
        if (input_title.text.toString().trim().isEmpty()) {
            input_layout_title.error = getString(R.string.err_msg_title)
            requestFocus(input_title)
            return false
        } else
            input_layout_title.isErrorEnabled = false
        return true
    }

    private fun validateYear(): Boolean {
        if (input_year.text.toString().trim().isEmpty()) {
            input_layout_year.error = getString(R.string.err_msg_year)
            requestFocus(input_year)
            return false
        } else
            input_layout_year.isErrorEnabled = false
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}