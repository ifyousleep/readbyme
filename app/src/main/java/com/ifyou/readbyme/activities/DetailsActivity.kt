package com.ifyou.readbyme.activities

import android.Manifest
import android.support.v7.app.AppCompatActivity
import android.app.DatePickerDialog
import android.os.Bundle
import android.os.Handler
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.support.v4.content.res.ResourcesCompat
import com.ifyou.readbyme.R
import com.ifyou.readbyme.db.BookModel
import com.ifyou.readbyme.utils.*
import com.ifyou.readbyme.db.DatabaseHelper
import com.ifyou.readbyme.db.DatabaseQueryHelper
import kotlinx.android.synthetic.main.activity_details.*
import org.jetbrains.anko.toast
import java.text.DateFormat
import java.util.*
import android.view.*
import android.support.v4.widget.NestedScrollView
import android.support.design.widget.CoordinatorLayout
import com.ifyou.readbyme.utils.DisableableAppBarLayoutBehavior

class DetailsActivity : AppCompatActivity() {

    private var bookId: Long = 0
    private var book: BookModel? = null
    private var tableName = "was_read"
    private var isEditor = false
    private val PICK_IMAGE_ID = 234
    private val STORAGE_PERMISSION = 1
    private var menu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        }

        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        collapsing_toolbar_layout.title = getString(R.string.title_details)
        collapsing_toolbar_layout.setExpandedTitleColor(ContextCompat.getColor(this, android.R.color.transparent))

        nameBookEdit.visibility = View.GONE
        nameAuthorEdit.visibility = View.GONE
        yearBookEdit.visibility = View.GONE
        noteBookEdit.visibility = View.GONE

        val extras = intent.extras
        if (extras != null) {
            bookId = extras.getLong("book")
            tableName = extras.getString("tableName")
            getBook()
        } else finish()

        photo.setOnClickListener {
            if (!hasStoragePermission())
                requestStoragePermission()
            else
                onPickImage()
        }

        fab.setOnClickListener {
            fabEdit()
        }

        unLockAppBar(true)

        if (!hasStoragePermission())
            requestStoragePermission()
    }

    private fun unLockAppBar(isLock: Boolean) {
        val layoutParams = appBarLayout.layoutParams as CoordinatorLayout.LayoutParams
        (layoutParams.behavior as DisableableAppBarLayoutBehavior).isEnabled = isLock
    }

    private fun getBook() {
        val sqlHelper = DatabaseHelper(this)
        val db = sqlHelper.readableDatabase
        book = DatabaseQueryHelper().findBooksWithId(db, bookId, tableName)
        db.close()

        if (!isEditor) {
            nameAuthor.text = book?.name
            nameBook.text = book?.title
            yearBook.text = book?.year
            noteBook.text = book?.notes

            if (book?.image != null) {
                photo.setImageBitmap(DbBitmapUtility.getImage(book?.image!!))
                plus.visibility = View.GONE
            } else {
                val color = randomColor(this)
                photo.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
            }

            when (tableName) {
                "was_read" -> showDateButton()
                "will_read" -> hideDateButton()
            }
        }
    }

    private fun onPickImage() {
        val chooseImageIntent = ImagePicker.getPickImageIntent(this)
        startActivityForResult(chooseImageIntent, PICK_IMAGE_ID)
    }

    private fun showDateButton() {
        val calendar = Calendar.getInstance()
        calendar.set(book?.startReadYear!!, book?.startReadMonth!!, book?.startReadDay!!)
        val startDate = DateFormat.getDateInstance().format(calendar.time)
        calendar.set(book?.finishReadYear!!, book?.finishReadMonth!!, book?.finishReadDay!!)
        val finishDate = DateFormat.getDateInstance().format(calendar.time)
        startRead.visibility = View.VISIBLE
        finishRead.visibility = View.VISIBLE
        arrow.visibility = View.VISIBLE
        startRead.text = startDate
        finishRead.text = finishDate
        startRead.setOnClickListener {
            showDialogDate(startRead)
        }
        finishRead.setOnClickListener {
            showDialogDate(finishRead)
        }
    }

    private fun hideDateButton() {
        startRead.visibility = View.GONE
        finishRead.visibility = View.GONE
        arrow.visibility = View.GONE
    }

    private fun showDialogDate(textView: TextView) {
        val now = Calendar.getInstance()
        val dialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            now.set(year, monthOfYear, dayOfMonth)
            val localeDate = DateFormat.getDateInstance().format(now.time)
            textView.text = localeDate
            val sqlHelper = DatabaseHelper(this)
            val db = sqlHelper.writableDatabase
            when (textView.id) {
                R.id.startRead -> DatabaseQueryHelper().updateStartReadItemWithId(db, bookId, tableName, year, monthOfYear, dayOfMonth)
                R.id.finishRead -> DatabaseQueryHelper().updateFinishReadItemWithId(db, bookId, tableName, year, monthOfYear, dayOfMonth)
            }
            db.close()
        }, now.get(Calendar.YEAR), now.get(Calendar.MONTH), now.get(Calendar.DAY_OF_MONTH))
        dialog.show()
    }

    private fun setIconShare(isShare: Boolean) {
        if (isShare)
            menu?.getItem(0)?.icon = ResourcesCompat.getDrawable(resources, R.drawable.ic_share, null)
        else
            menu?.getItem(0)?.icon = ResourcesCompat.getDrawable(resources, R.drawable.ic_check, null)
    }

    private fun hideAllEdit() {
        isEditor = false
        hideSoftInput()
        deactivateEditor(noteBookEdit, noteBook)
        deactivateEditor(yearBookEdit, yearBook)
        deactivateEditor(nameBookEdit, nameBook)
        deactivateEditor(nameAuthorEdit, nameAuthor)
        if (tableName == "was_read") showDateButton()

        nestedScroll.fullScroll(NestedScrollView.FOCUS_UP)
        appBarLayout.setExpanded(true, true)
        unLockAppBar(true)
        coordinatorLayout.addView(fab)

        setIconShare(true)
        updateDb()
    }

    private fun fabEdit() {
        isEditor = true
        appBarLayout.setExpanded(false, true)
        unLockAppBar(false)
        coordinatorLayout.removeView(fab)
        setIconShare(false)
        getBook()
        activateEditor(nameAuthor, nameAuthorEdit, book?.name!!)
        activateEditor(nameBook, nameBookEdit, book?.title!!)
        activateEditor(yearBook, yearBookEdit, book?.year!!)
        activateEditor(noteBook, noteBookEdit, book?.notes!!)
        Handler().postDelayed({
            nameAuthorEdit.requestFocus()
            showSoftInput(nameAuthorEdit)
        }, 10)
        if (tableName == "was_read") hideDateButton()
    }

    private fun validateEditText(fields: Array<EditText>): Boolean {
        if (fields.filter { it.text.isEmpty() }.isNotEmpty())
            return false
        return true
    }

    private fun fabCheck() {
        val arrayOfEditText = arrayOf<EditText>(nameAuthorEdit, nameBookEdit, yearBookEdit)
        if (validateEditText(arrayOfEditText)) {
            hideAllEdit()
        } else
            toast(R.string.err_msg_empty)
    }

    private fun activateEditor(hideView: View, showView: EditText, text: String) {
        hideView.visibility = View.GONE
        showView.setText(text)
        showView.visibility = View.VISIBLE
        showView.setOnEditorActionListener { textView, i, keyEvent ->
            onEditorAction(i)
        }
    }

    private fun deactivateEditor(hideView: EditText, showView: TextView) {
        hideView.visibility = View.GONE
        showView.text = hideView.text.toString()
        showView.visibility = View.VISIBLE
    }

    private fun onEditorAction(actionId: Int): Boolean {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            hideAllEdit()
            return true
        }
        return false
    }

    private fun updateDb() {
        val sqLiteHelper = DatabaseQueryHelper()
        val db = DatabaseHelper(this).writableDatabase
        sqLiteHelper.updateAuthorTitleYearNotesItemWithId(db, bookId, tableName,
                nameAuthor.text.toString(), nameBook.text.toString(),
                yearBook.text.toString(), noteBook.text.toString())
        db.close()
    }

    override fun onBackPressed() {
        hideSoftInput()
        val intentParent = intent
        setResult(10002, intentParent)
        super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.details_menu, menu)
        this.menu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            R.id.action_share -> {
                if (isEditor) {
                    fabCheck()
                } else {
                    if (hasStoragePermission()) {
                        shareBookAsImage()
                    } else
                        requestStoragePermission()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun shareBookAsImage() {
        val bitmap = Bitmap.createBitmap(bodyBook.width, bodyBook.height, Bitmap.Config.ARGB_8888)
        bitmap.eraseColor(Color.WHITE)
        val canvas = Canvas(bitmap)
        bodyBook.draw(canvas)
        val newFile = MediaStore.Images.Media.insertImage(contentResolver, bitmap, null, null)
        val contentUri = Uri.parse(newFile)
        if (contentUri != null) {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.setDataAndType(contentUri, contentResolver.getType(contentUri))
            shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri)
            startActivity(Intent.createChooser(shareIntent, getString(R.string.choose_app)))

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val bitmap: Bitmap?
        when (requestCode) {
            PICK_IMAGE_ID -> {
                bitmap = ImagePicker.getImageFromResult(this, resultCode, data)
                if (bitmap != null) {
                    val sqLiteHelper = DatabaseQueryHelper()
                    val db = DatabaseHelper(this).writableDatabase
                    sqLiteHelper.updateCoverItemWithId(db, bookId, tableName, DbBitmapUtility.getBytes(bitmap))
                    db.close()
                    recreate()
                }
            }
            else -> super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun requestStoragePermission() = ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), STORAGE_PERMISSION)
    private fun hasStoragePermission() = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == STORAGE_PERMISSION && grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            toast(R.string.success)
        }
    }
}