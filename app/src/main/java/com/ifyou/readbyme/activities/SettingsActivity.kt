package com.ifyou.readbyme.activities

import android.support.v7.app.AppCompatActivity
import android.support.v4.content.ContextCompat
import android.view.MenuItem
import android.os.Bundle
import com.ifyou.readbyme.R
import com.ifyou.readbyme.fragments.PreferenceFragment
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorWhite))
        setSupportActionBar(toolbar)

        supportActionBar!!.title = getString(R.string.action_settings)

        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.container, PreferenceFragment()).commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
