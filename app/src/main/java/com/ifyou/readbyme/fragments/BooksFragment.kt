package com.ifyou.readbyme.fragments

import android.content.Intent
import android.graphics.Canvas
import android.graphics.Paint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.support.v7.app.AlertDialog
import com.ifyou.readbyme.R
import com.ifyou.readbyme.adapters.BooksAdapter
import com.ifyou.readbyme.db.DatabaseHelper
import com.ifyou.readbyme.db.BookModel
import com.ifyou.readbyme.db.DatabaseQueryHelper
import kotlinx.android.synthetic.main.fragment_main.*
import android.support.v7.widget.helper.ItemTouchHelper
import android.support.v7.widget.RecyclerView
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.ifyou.readbyme.activities.DetailsActivity
import org.jetbrains.anko.support.v4.withArguments

class BooksFragment : Fragment() {

    private var adapter: BooksAdapter? = null
    private var rv: LinearLayoutManager? = null
    private val ALPHA_FULL = 1.0f
    private var tableName = ""
    private var sortBy = 0
    private val UPDATE_FRAGMENT_ID = 10002

    companion object {
        fun newInstance(tableName: String = "", sortBy: Int = 0): BooksFragment {
            return BooksFragment().withArguments("tableName" to tableName, "sortBy" to sortBy)
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater?.inflate(R.layout.fragment_main, container, false)
        return v
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tableName = arguments.getString("tableName")
        sortBy = arguments.getInt("sortBy")
        val books: MutableList<BookModel>
        when (sortBy) {
            0 -> books = getAllBooks()
            1 -> books = getAllBooksByAuthor()
            else -> books = getAllBooksByTitle()
        }
        if (books.isEmpty()) {
            booksEmpty()
        } else {
            booksNotEmpty()
            recyclerView.setHasFixedSize(true)
            rv = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            recyclerView.layoutManager = rv
            adapter = BooksAdapter(activity, books, tableName)
            recyclerView.adapter = adapter
            adapter?.onClickListener = object : BooksAdapter.OnClickListener {
                override fun onClick(view: View, book: BookModel) {
                    val intent = Intent(context, DetailsActivity::class.java)
                    intent.putExtra("book", book.id)
                    intent.putExtra("tableName", tableName)
                    startActivityForResult(intent, UPDATE_FRAGMENT_ID)
                }
            }
            val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
            itemTouchHelper.attachToRecyclerView(recyclerView)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == UPDATE_FRAGMENT_ID) {
            fragmentManager
                    .beginTransaction()
                    .detach(this)
                    .attach(this)
                    .commit()
        }
        //(activity as MainActivity).setStatusBarAndToolbarColor()
    }

    private fun booksEmpty() {
        recyclerView.visibility = View.GONE
        textEmpty.visibility = View.VISIBLE
    }

    private fun booksNotEmpty() {
        textEmpty.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
    }

    private fun deleteDialog(pos: Int) {
        AlertDialog.Builder(activity)
                .setMessage(R.string.dialog_msg)
                .setPositiveButton(R.string.dialog_ok) { dialog, id -> deleteBooks(pos) }
                .setNegativeButton(R.string.dialog_no) { dialog, id -> adapter?.notifyItemChanged(pos) }
                .show()
    }

    private fun favoriteBooks(pos: Int) {
        val sqlHelper = DatabaseHelper(context)
        val db = sqlHelper.writableDatabase
        var isLike = 1
        val books = DatabaseQueryHelper().queryGetAllBooks(db, tableName)
        if (books[pos].like == 1)
            isLike = 0
        DatabaseQueryHelper().updateFavoriteItemWithId(db, books[pos].id!!, tableName, isLike)
        db.close()
        adapter?.updateFavoriteItem(pos, isLike)
        adapter?.notifyItemChanged(pos)
    }

    private fun getAllBooks(): MutableList<BookModel> {
        val sqlHelper = DatabaseHelper(context)
        val db = sqlHelper.readableDatabase
        val books = DatabaseQueryHelper().queryGetAllBooks(db, tableName)
        db.close()
        return books
    }

    private fun getAllBooksByAuthor(): MutableList<BookModel> {
        val sqlHelper = DatabaseHelper(context)
        val db = sqlHelper.readableDatabase
        val books = DatabaseQueryHelper().queryGetAllBooksByAuthor(db, tableName)
        db.close()
        return books
    }

    private fun getAllBooksByTitle(): MutableList<BookModel> {
        val sqlHelper = DatabaseHelper(context)
        val db = sqlHelper.readableDatabase
        val books = DatabaseQueryHelper().queryGetAllBooksByTitle(db, tableName)
        db.close()
        return books
    }

    private fun deleteBooks(pos: Int) {
        val sqlHelper = DatabaseHelper(context)
        val db = sqlHelper.readableDatabase
        val books = DatabaseQueryHelper().queryGetAllBooks(db, tableName)
        DatabaseQueryHelper().deleteItemWithId(db, books[pos].id!!, tableName)
        db.close()
        adapter?.removeItemAtPosition(pos)
        adapter?.notifyItemRemoved(pos)
        if (adapter?.itemCount == 0)
            booksEmpty()
    }

    private fun moveToRead(pos: Int) {
        val sqlHelper = DatabaseHelper(context)
        val db = sqlHelper.readableDatabase
        val books = DatabaseQueryHelper().queryGetAllBooks(db, tableName)
        DatabaseQueryHelper().moveToWasItemWithId(db, books[pos].id!!, tableName)
        db.close()
        adapter?.removeItemAtPosition(pos)
        adapter?.notifyItemRemoved(pos)
        if (adapter?.itemCount == 0)
            booksEmpty()
    }

    private fun leftSwipe(pos: Int) {
        when (tableName) {
            "was_read" -> favoriteBooks(pos)
            "will_read" -> moveToRead(pos)
        }
    }

    var simpleItemTouchCallback: ItemTouchHelper.SimpleCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
            return false
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, swipeDir: Int) {
            if (swipeDir == ItemTouchHelper.RIGHT) {
                deleteDialog(viewHolder.adapterPosition)
            } else
                leftSwipe(viewHolder.adapterPosition)
        }

        override fun onMoved(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, fromPos: Int, target: RecyclerView.ViewHolder, toPos: Int, x: Int, y: Int) {
            super.onMoved(recyclerView, viewHolder, fromPos, target, toPos, x, y)
        }

        override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
            if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                val itemView = viewHolder.itemView
                val p = Paint()
                val icon: Bitmap
                if (dX > 0) {
                    icon = BitmapFactory.decodeResource(context.resources, R.drawable.ic_delete_sweep)
                    p.setARGB(255, 244, 67, 54)
                    c.drawRect(itemView.left.toFloat(), itemView.top.toFloat(), dX,
                            itemView.bottom.toFloat(), p)
                    if (dX > 96f + icon.width) {
                        val height = itemView.height / 2 - icon.height / 2
                        c.drawBitmap(icon, 96f, itemView.top.toFloat() + height, null)
                    }
                } else {
                    when (tableName) {
                        "was_read" -> icon = BitmapFactory.decodeResource(context.resources, R.drawable.ic_favorite)
                        else -> icon = BitmapFactory.decodeResource(context.resources, R.drawable.ic_add_check)
                    }

                    p.setARGB(255, 76, 175, 80)
                    c.drawRect(itemView.right.toFloat() + dX, itemView.top.toFloat(),
                            itemView.right.toFloat(), itemView.bottom.toFloat(), p)
                    if (dX < -1 * (96 + icon.width)) {
                        val height = itemView.height / 2 - icon.height / 2
                        c.drawBitmap(icon, itemView.right.toFloat() - icon.width - 96f, itemView.top.toFloat() + height, null)
                    }
                }
                val alpha = ALPHA_FULL - Math.abs(dX) / viewHolder.itemView.width.toFloat()
                viewHolder.itemView.alpha = alpha
                viewHolder.itemView.translationX = dX
            } else {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }
        }
    }
}
