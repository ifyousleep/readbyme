package com.ifyou.readbyme.fragments

import android.os.Bundle
import android.support.v7.preference.PreferenceFragmentCompat
import android.view.View
import com.ifyou.readbyme.R
import com.ifyou.readbyme.db.DatabaseHelper
import com.ifyou.readbyme.db.DatabaseQueryHelper
import java.io.File

class PreferenceFragment : PreferenceFragmentCompat() {

    private val PREF_CLEAR_BD = "pref_clear_bd"
    private var mDbFile: File? = null

    override fun onCreatePreferences(savedInstanceState: Bundle?, p1: String?) {
        addPreferencesFromResource(R.xml.preferences)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val clearBd = findPreference(PREF_CLEAR_BD)
        clearBd.summary = getSizeBd()
        clearBd.setOnPreferenceClickListener {
            val sqlHelper = DatabaseHelper(context)
            val db = sqlHelper.writableDatabase
            DatabaseQueryHelper().dropBd(db)
            db.close()
            clearBd.summary = getSizeBd()
            true
        }
    }

    private fun getSizeBd(): String {
        val sqlHelper = DatabaseHelper(context)
        mDbFile = File(context.getDatabasePath(sqlHelper.databaseName).toURI())
        val sizeBd = mDbFile?.length()?.div(1024)
        val value: String
        if (sizeBd!! >= 1024)
            value = (sizeBd / 1024).toString() + " Mb"
        else
            value = sizeBd.toString() + " Kb"
        return value
    }
}
