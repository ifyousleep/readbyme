package com.ifyou.readbyme.db

import android.database.sqlite.SQLiteOpenHelper
import android.database.sqlite.SQLiteDatabase
import android.content.Context

class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, DatabaseHelper.DATABASE_NAME, null, DatabaseHelper.SCHEMA) {

    companion object {
        private val DATABASE_NAME = "ReadMyBooks.db" // название бд
        private val SCHEMA = 1 // версия базы данных
    }

    override fun onCreate(db: SQLiteDatabase?) {
        DatabaseQueryHelper().createTableWas(db)
        DatabaseQueryHelper().createTableWill(db)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {

    }
}
