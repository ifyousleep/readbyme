package com.ifyou.readbyme.db

import java.util.*


data class BookModel(
        val name: String = "",
        val title: String = "",
        val year: String = "",
        var like: Int = 0,
        val startReadYear: Int = 0,
        val startReadMonth: Int = 0,
        val startReadDay: Int = 0,
        val finishReadYear: Int = 0,
        val finishReadMonth: Int = 0,
        val finishReadDay: Int = 0,
        val notes: String = "",
        val image: ByteArray? = null,
        var id: Long? = null
) {
    override fun hashCode(): Int = Arrays.hashCode(image)

    override fun equals(other: Any?): Boolean = when (other) {
        is ByteArray -> Arrays.equals(image, other)
        else -> false
    }
}