package com.ifyou.readbyme.db

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.provider.BaseColumns
import java.util.*
import kotlin.comparisons.compareBy

class DatabaseQueryHelper {

    private val was_readTable = "was_read"
    private val will_readTable = "will_read"
    private val name = "name"
    private val title = "title"
    private val year = "year"
    private val like = "like"
    private val startReadDay = "startReadDay"
    private val startReadMonth = "startReadMonth"
    private val startReadYear = "startReadYear"
    private val finishReadDay = "finishReadDay"
    private val finishReadMonth = "finishReadMonth"
    private val finishReadYear = "finishReadYear"
    private val notes = "notes"
    private val cover = "cover"
    private val stats= "stats"

    fun createTableWas(db: SQLiteDatabase?) {
        db?.execSQL("""
            CREATE TABLE $was_readTable(
                ${BaseColumns._ID} INTEGER PRIMARY KEY AUTOINCREMENT,
                $year TEXT,
                $name TEXT,
                $title TEXT,
                $like INTEGER,
                $startReadDay INTEGER,
                $startReadMonth INTEGER,
                $startReadYear INTEGER,
                $finishReadDay INTEGER,
                $finishReadMonth INTEGER,
                $finishReadYear INTEGER,
                $notes TEXT,
                $cover BLOB,
                $stats TEXT
            )
        """)
    }

    fun createTableWill(db: SQLiteDatabase?) {
        db?.execSQL("""
            CREATE TABLE $will_readTable(
                ${BaseColumns._ID} INTEGER PRIMARY KEY AUTOINCREMENT,
                $year TEXT,
                $name TEXT,
                $title TEXT,
                $like INTEGER,
                $startReadDay INTEGER,
                $startReadMonth INTEGER,
                $startReadYear INTEGER,
                $finishReadDay INTEGER,
                $finishReadMonth INTEGER,
                $finishReadYear INTEGER,
                $notes TEXT,
                $cover BLOB,
                $stats TEXT
            )
        """)
    }

    fun insertItem(db: SQLiteDatabase, book: BookModel, nameBase: String): Long {
        val now = Calendar.getInstance()
        val values = ContentValues()
        values.put(name, book.name)
        values.put(title, book.title)
        values.put(year, book.year)
        values.put(like, book.like)
        values.put(startReadDay, now.get(Calendar.YEAR))
        values.put(startReadMonth, now.get(Calendar.MONTH))
        values.put(startReadYear, now.get(Calendar.DAY_OF_MONTH))
        values.put(finishReadDay, now.get(Calendar.YEAR))
        values.put(finishReadMonth, now.get(Calendar.MONTH))
        values.put(finishReadYear, now.get(Calendar.DAY_OF_MONTH))
        values.put(cover, book.image)
        values.put(notes, book.notes)
        return db.insert(nameBase, null, values)
    }

    fun deleteItemWithId(db: SQLiteDatabase, id: Long, nameBase: String) {
        db.delete(nameBase, "${BaseColumns._ID} = ?", arrayOf(id.toString()))
    }

    fun dropBd(db: SQLiteDatabase) {
        db.delete(was_readTable, null, null)
        db.delete(will_readTable, null, null)
    }

    fun updateFavoriteItemWithId(db: SQLiteDatabase, id: Long, nameBase: String, isLike: Int) {
        val values = ContentValues()
        values.put(like, isLike)
        db.update(nameBase, values, "${BaseColumns._ID} = ?", arrayOf(id.toString()))
    }

    fun updateStartReadItemWithId(db: SQLiteDatabase, id: Long, nameBase: String, newStartReadDay: Int, newStartReadMonth: Int, newStartReadYear: Int) {
        val values = ContentValues()
        values.put(startReadDay, newStartReadDay)
        values.put(startReadMonth, newStartReadMonth)
        values.put(startReadYear, newStartReadYear)
        db.update(nameBase, values, "${BaseColumns._ID} = ?", arrayOf(id.toString()))
    }

    fun updateFinishReadItemWithId(db: SQLiteDatabase, id: Long, nameBase: String, newFinishReadDay: Int, newFinishReadMonth: Int, newFinishReadYear: Int) {
        val values = ContentValues()
        values.put(finishReadDay, newFinishReadDay)
        values.put(finishReadMonth, newFinishReadMonth)
        values.put(finishReadYear, newFinishReadYear)
        db.update(nameBase, values, "${BaseColumns._ID} = ?", arrayOf(id.toString()))
    }

    fun updateAuthorTitleYearNotesItemWithId(db: SQLiteDatabase, id: Long, nameBase: String,
                                             author: String, newTitle: String, newYear: String, newNote: String) {
        val values = ContentValues()
        values.put(name, author)
        values.put(title, newTitle)
        values.put(year, newYear)
        values.put(notes, newNote)
        db.update(nameBase, values, "${BaseColumns._ID} = ?", arrayOf(id.toString()))
    }

    fun updateCoverItemWithId(db: SQLiteDatabase, id: Long, nameBase: String, newCover: ByteArray) {
        val values = ContentValues()
        values.put(cover, newCover)
        db.update(nameBase, values, "${BaseColumns._ID} = ?", arrayOf(id.toString()))
    }

    fun moveToWasItemWithId(db: SQLiteDatabase, id: Long, nameBase: String) {
        val book = findBooksWithId(db, id, nameBase)
        insertItem(db, book!!, "was_read")
        deleteItemWithId(db, id, nameBase)
    }

    fun findBooksWithId(db: SQLiteDatabase, id: Long, nameBase: String): BookModel? {
        val cursor = db.query(
                nameBase, null, "${BaseColumns._ID} = ?", arrayOf(id.toString()), null, null, null)
        if (cursor.moveToFirst()) {
            return toBook(cursor)
        }
        return null
    }

    fun queryGetAllBooks(db: SQLiteDatabase, nameBase: String): MutableList<BookModel> {
        val cursor = db.query(nameBase, null, null, null, null, null, null)
        val books = ArrayList<BookModel>()
        while (cursor.moveToNext()) {
            books.add(toBook(cursor))
        }
        return books.asReversed()
    }

    fun queryGetAllBooksByAuthor(db: SQLiteDatabase, nameBase: String): MutableList<BookModel> {
        val cursor = db.query(nameBase, null, null, null, null, null, null)
        val books = ArrayList<BookModel>()
        while (cursor.moveToNext()) {
            books.add(toBook(cursor))
        }
        books.sortWith(compareBy { it.name })
        return books
    }

    fun queryGetAllBooksByTitle(db: SQLiteDatabase, nameBase: String): MutableList<BookModel> {
        val cursor = db.query(nameBase, null, null, null, null, null, null)
        val books = ArrayList<BookModel>()
        while (cursor.moveToNext()) {
            books.add(toBook(cursor))
        }
        books.sortWith(compareBy { it.title })
        return books
    }

    private fun toBook(cursor: Cursor): BookModel {
        return BookModel(
                cursor.getString(cursor.getColumnIndex(name)),
                cursor.getString(cursor.getColumnIndex(title)),
                cursor.getString(cursor.getColumnIndex(year)),
                cursor.getInt(cursor.getColumnIndex(like)),
                cursor.getInt(cursor.getColumnIndex(startReadDay)),
                cursor.getInt(cursor.getColumnIndex(startReadMonth)),
                cursor.getInt(cursor.getColumnIndex(startReadYear)),
                cursor.getInt(cursor.getColumnIndex(finishReadDay)),
                cursor.getInt(cursor.getColumnIndex(finishReadMonth)),
                cursor.getInt(cursor.getColumnIndex(finishReadYear)),
                cursor.getString(cursor.getColumnIndex(notes)),
                cursor.getBlob(cursor.getColumnIndex(cover)),
                cursor.getLong(cursor.getColumnIndex(BaseColumns._ID)))
    }
}
