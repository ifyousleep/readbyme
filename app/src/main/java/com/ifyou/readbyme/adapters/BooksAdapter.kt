package com.ifyou.readbyme.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import com.ifyou.readbyme.R
import com.ifyou.readbyme.db.BookModel
import kotlinx.android.synthetic.main.adapter_main_item.view.*
import android.graphics.PorterDuff
import com.ifyou.readbyme.utils.randomColor
import java.text.DateFormat
import java.util.*

class BooksAdapter(val context: Context, val data: MutableList<BookModel>, tableName: String)
    : RecyclerView.Adapter<BooksAdapter.ViewHolder>() {

    var onClickListener: BooksAdapter.OnClickListener? = null
    val tName = tableName

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BooksAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.adapter_main_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: BooksAdapter.ViewHolder, position: Int) {
        holder.bindData(data[position], context, tName)
        holder.itemView.setOnClickListener {
            onClickListener?.onClick(it, data[position])
        }
    }

    override fun getItemCount() = data.size

    fun removeItemAtPosition(position: Int) {
        data.removeAt(position)
    }

    fun updateFavoriteItem(position: Int, isLike: Int) {
        data[position].like = isLike
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindData(item: BookModel, context: Context, tableName: String) {
            val color = randomColor(context)
            val calendar = Calendar.getInstance()
            calendar.set(item.startReadYear, item.startReadMonth, item.startReadDay)
            val startDate = DateFormat.getDateInstance().format(calendar.time)
            calendar.set(item.finishReadYear, item.finishReadMonth, item.finishReadDay)
            val finishDate = DateFormat.getDateInstance().format(calendar.time)
            val startToFinish = startDate + " - " + finishDate
            itemView.avatar.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
            itemView.avatar_text.text = item.name[0].toString()
            itemView.itemName.text = item.name
            itemView.itemTitle.text = item.title
            when (tableName) {
                "was_read" -> itemView.itemYear.text = startToFinish
                "will_read" -> itemView.itemYear.text = item.year
            }
            if (item.like == 0)
                itemView.icon.visibility = View.GONE
            else {
                itemView.icon.visibility = View.VISIBLE
                itemView.icon.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
            }
        }
    }

    interface OnClickListener {
        fun onClick(view: View, book: BookModel)
    }
}
