package com.ifyou.readbyme.preferences

import android.content.Context
import com.ifyou.readbyme.helpers.PreferencesHelper


object Preferences {

    fun getPreferences(context: Context): PreferencesHelper {
        return PreferencesHelper(context)
    }

}
